using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using System.IO;
using Microsoft.Win32;


//This script compares structures with same name from same patient same image session but two difference structuresets
// using Hausdorff distance 95%, Dice overlap, surface Dice 1mm and 2 mm
// version 1.0 15-09-2023
// Requires input of text file


// TODO: Replace the following version attributes by creating AssemblyInfo.cs. You can do this in the properties of the Visual Studio project.
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
[assembly: AssemblyInformationalVersion("1.0")]

// TODO: Uncomment the following line if the script requires write access.
 [assembly: ESAPIScript(IsWriteable = true)]

namespace recursive_AI_metric_evaluation
{   
  class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      try
      {
        using (Application app = Application.CreateApplication())
        {
          Execute(app);
        }
      }
      catch (Exception e)
      {
        Console.Error.WriteLine(e.ToString());
      }
    }
    static void Execute(Application app)
    {
            // TODO: Add your code here.
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set the properties of the OpenFileDialog
            openFileDialog.InitialDirectory = @"\\Client\C$"; // Set the initial directory
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"; // Set file filters
            openFileDialog.FilterIndex = 1; // Set the default filter index
            openFileDialog.RestoreDirectory = true; // Restore the last selected directory

            // Show the OpenFileDialog and get the result
            bool? dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                string selectedFilePath = openFileDialog.FileName;
                Console.WriteLine("Selected File: " + selectedFilePath);
            
            string[] linesPtLabelID = System.IO.File.ReadAllLines(selectedFilePath); // load list of patients for analysis
            string[] outputdir = linesPtLabelID.ElementAt(0).Split(';');  //pull patientUD from list
            Console.WriteLine("Exporting here: " + outputdir[1]);
            System.IO.Directory.CreateDirectory(outputdir[1]);  // creating the exoort directory
            System.IO.File.GetAttributes(outputdir[1]);
            //foreach (string listptID in linesPtLabelID)  //iterate through all patients
            int ptnumCount = linesPtLabelID.Count();
            for (int ptnum = 1; ptnum < ptnumCount; ptnum++)
            {
                string[] ptIDstr = linesPtLabelID.ElementAt(ptnum).Split(';');  //pull patientUD from list
                Patient patient = app.OpenPatientById(ptIDstr[0]); //open patient in DB
                if ((patient != null))//If the patient exists perform analysis
                {
                    IEnumerable<StructureSet> structureSets = patient.StructureSets;
                    patient.BeginModifications();

                    // Find the StructureSet by name
                    StructureSet structureSetGT = FindStructureSetBySubstring(structureSets, ptIDstr[1]); // Identifying a structureset containing substring matching ptIDstr[1]
                    StructureSet structureSetAI = FindStructureSetByName(structureSets, "RTSTRUCT"); // Identifying a structureset matching RTSTRUCT

                    //StructureSet structureSetGT = FindStructureSetBySubstring(structureSets, "Prim�r"); // Replace with the substring of the StructureSet you want to find
                    //bool isBodyApproved;
                    //isBodyApproved = structureSetGT.Any(strct => strct.Id.ToUpper().Contains("BODY") && strct.IsApproved == true);
                    var csvMetricsPt = new StringBuilder(); //creating csv holder for output

                    string lineID = string.Format("{0}", patient.Id);
                    //Console.WriteLine(lineID);

                    if (structureSetAI != null && structureSetAI.Image.FOR == structureSetGT.Image.FOR) //Checking they have same FrameOfReference
                    {
                        string line = string.Format("{0},{1},{2},{3},{4},{5}", structureSetAI.Id, structureSetGT.Id, "HD95", "DSC", "sDSC_1mm", "sDSC_2mm"); //initial the output file for this patient. ); //initial the output file for this patient. 
                        csvMetricsPt.AppendLine(line);
                        Console.WriteLine(line);

                        Structure targetAI = null;
                        int maxnum = structureSetAI.Structures.Count(); //find number of structures in the AI structureset
                            for (int i = 0; i < maxnum; i++) // loops through all structures in the AI structureset
                            { 
                            Structure structureAI = structureSetAI.Structures.ElementAt(i);
                            targetAI = structureAI;
                            Structure targetGT = null;
                            bool foundstructure = false;
                            foreach (var structureGT in structureSetGT.Structures) 
                            {
                                if (structureGT.Id == targetAI.Id && structureGT.Id != "BODY")//find the corresponding structure in the approved structureset and exclude Body since it takes too long to analyse
                                {
                                    targetGT = structureGT;
                                    if (targetGT.Volume>0.1 && targetAI.Volume>0.1) // if both structure are not empty then continue
                                    { 

                                    
                                    List <Point> contourPointsListGT = new List<Point>();
                                    for (int j = 0; j < targetGT.MeshGeometry.Positions.Count(); j++)
                                    {
                                        VVector vectorDICOM = new VVector();
                                        vectorDICOM.x = targetGT.MeshGeometry.Positions[j].X;
                                        vectorDICOM.y = targetGT.MeshGeometry.Positions[j].Y;
                                        vectorDICOM.z = targetGT.MeshGeometry.Positions[j].Z;
                                        contourPointsListGT.Add(new Point(vectorDICOM.x, vectorDICOM.y, vectorDICOM.z));
                                    }
                                    List<Point> contourPointsListAI = new List<Point>();
                                    for (int j = 0; j < targetAI.MeshGeometry.Positions.Count(); j++)
                                    {
                                        VVector vectorDICOM = new VVector();
                                        vectorDICOM.x = targetAI.MeshGeometry.Positions[j].X;
                                        vectorDICOM.y = targetAI.MeshGeometry.Positions[j].Y;
                                        vectorDICOM.z = targetAI.MeshGeometry.Positions[j].Z;
                                        contourPointsListAI.Add(new Point(vectorDICOM.x, vectorDICOM.y, vectorDICOM.z));
                                    }

                                    double hausdorff95 = Hausdorff95(contourPointsListGT, contourPointsListAI);
                                    //double DICE = DICEfunc(contourPointsListGT, contourPointsListAI);

                                    //needs cleaning up of naming of the tempirary structures 
                                    Structure structureSub1 = structureSetAI.AddStructure("CONTROL", "sub1");
                                    Structure structureSub2 = structureSetAI.AddStructure("CONTROL", "sub2");
                                    Structure structureSub3 = structureSetAI.AddStructure("CONTROL", "sub3");
                                    Structure structureSub4 = structureSetAI.AddStructure("CONTROL", "sub4");
                                    Structure structureSub5 = structureSetAI.AddStructure("CONTROL", "sub5");
                                    Structure structureSub6 = structureSetAI.AddStructure("CONTROL", "sub6");

                                    structureSub3.ConvertToHighResolution();
                                    structureSub4.ConvertToHighResolution();

                                    // Here we copy the GT structure to the AI structureset new structure called sub3
                                    IEnumerable<int>  GTbounds = _GetMeshBounds(targetGT, structureSetGT);
                                    foreach (var item in GTbounds)
                                    {
                                        VVector[][] contours = targetGT.GetContoursOnImagePlane(item);
                                        foreach (var item2 in contours)
                                        {
                                            structureSub3.AddContourOnImagePlane(item2, item);
                                        }
                                        
                                    }
                                    // Here we copy the AI structure into new structure called sub4 just in case
                                    structureSub4.SegmentVolume = targetAI.SegmentVolume;

                                
                                    // making sure both structures are of same type of resolution
                                    if (structureSub3.IsHighResolution != structureSub4.IsHighResolution)
                                    { 
                                        if(structureSub3.IsHighResolution == true)
                                        {
                                            structureSub4.ConvertToHighResolution();
                                        }
                                        else
                                        {
                                            structureSub3.ConvertToHighResolution();
                                        }
                                    }
                                
                                    // loading the union of sub 3 and sub4 into the sub2 structure and calculating DICE SIMILARITY score
                                    structureSub2.SegmentVolume = structureSub3.And(structureSub4.SegmentVolume);                                    
                                    double union = structureSub2.Volume; // - structureS.ub1.Volume - structureSub2.Volume;
                                    double DSC = 2 * union / (targetGT.Volume + targetAI.Volume);
                                
                                    // Calculating sDSC 1 mm in the same way as above
                                    structureSub1.SegmentVolume = structureSub3.Margin(1.0);
                                    structureSub2.SegmentVolume = structureSub3.Margin(-1.0);
                                    structureSub5.SegmentVolume = structureSub1.Sub(structureSub2.SegmentVolume);
                                    structureSub1.SegmentVolume = structureSub4.Margin(1.0);
                                    structureSub2.SegmentVolume = structureSub4.Margin(-1.0);
                                    structureSub6.SegmentVolume = structureSub1.Sub(structureSub2.SegmentVolume);
                                                                            
                                    structureSub1.SegmentVolume = structureSub5.And(structureSub6.SegmentVolume);
                                    double union_sDSC = structureSub1.Volume; // - structureS.ub1.Volume - structureSub2.Volume;
                                    double sDSC_1mm = 2 * union_sDSC / (structureSub5.Volume + structureSub6.Volume);

                                    // Calculating sDSC 2 mm in the same way as above
                                    structureSub1.SegmentVolume = structureSub3.Margin(2.0);
                                    structureSub2.SegmentVolume = structureSub3.Margin(-2.0);
                                    structureSub5.SegmentVolume = structureSub1.Sub(structureSub2.SegmentVolume);
                                    structureSub1.SegmentVolume = structureSub4.Margin(2.0);
                                    structureSub2.SegmentVolume = structureSub4.Margin(-2.0);
                                    structureSub6.SegmentVolume = structureSub1.Sub(structureSub2.SegmentVolume);

                                    structureSub1.SegmentVolume = structureSub5.And(structureSub6.SegmentVolume);
                                    double union_sDSC2 = structureSub1.Volume; // - structureS.ub1.Volume - structureSub2.Volume;
                                    double sDSC_2mm = 2 * union_sDSC2 / (structureSub5.Volume + structureSub6.Volume);
                                        
                                    structureSetAI.RemoveStructure(structureSub1);
                                    structureSetAI.RemoveStructure(structureSub2);
                                    structureSetAI.RemoveStructure(structureSub3);
                                    structureSetAI.RemoveStructure(structureSub4);
                                    structureSetAI.RemoveStructure(structureSub5);
                                    structureSetAI.RemoveStructure(structureSub6);
                                    string line2 = string.Format("{0},{1},{2},{3},{4},{5}", patient.Id, targetAI.Id, hausdorff95.ToString(("0.###")), DSC.ToString(("0.###")), sDSC_1mm.ToString(("0.###")), sDSC_2mm.ToString(("0.###")));
                                    csvMetricsPt.AppendLine(line2);
                                    Console.WriteLine(line2);
                                    foundstructure = true;
                                }
                                if (foundstructure == true)
                                    { break; }
                                }
                        
                            }
                        }
                        // StructureSet found, do something with it
                        Console.WriteLine($"StructureSet found: Name = {structureSetAI.Id}");
                        string filenameUncert = string.Format(@"{0}\{1}-AI_metrics.csv", outputdir[1], patient.Id);
                        File.AppendAllText(filenameUncert, csvMetricsPt.ToString());
                    }
                    else
                    {
                        Console.WriteLine("StructureSet not found.");
                    }

                    // Clean up resources
                    app.ClosePatient();
                    //app.Dispose();
                }
            }
            }


        }

        static int _GetSlice(double z, StructureSet SS)
    {
        var imageRes = SS.Image.ZRes;
        return Convert.ToInt32((z - SS.Image.Origin.z) / imageRes);
    }

    static IEnumerable<int> _GetMeshBounds(Structure structure, StructureSet SS)
    {
        var mesh = structure.MeshGeometry.Bounds;
        var meshLow = _GetSlice(mesh.Z, SS);
        var meshUp = _GetSlice(mesh.Z + mesh.SizeZ, SS) + 1; return Enumerable.Range(meshLow, meshUp);
    }



    static StructureSet FindStructureSetByName(IEnumerable<StructureSet> structureSets, string name)
    {
            // Find the StructureSet by name
        return structureSets.FirstOrDefault(s => s.Id.Equals(name, StringComparison.OrdinalIgnoreCase));
    }
    static StructureSet FindStructureSetBySubstring(IEnumerable<StructureSet> structureSets, string substring)
    {
            // Find the StructureSet by substring in the ID
        return structureSets.FirstOrDefault(s => s.Id.IndexOf(substring, StringComparison.OrdinalIgnoreCase) >= 0);
    }

       // private static double Hausdorff95(List<double[]> contourPointsListGT, List<double[]> contourPointsListAI)
        //{
        //    throw new NotImplementedException();
       

    private static double Hausdorff95(List<Point> referencePoints, List<Point> comparePoints)
        {
            List<double> distances = new List<double>();

            foreach (Point referencePoint in referencePoints)
            {
                double minDistance = double.MaxValue;

                foreach (Point comparePoint in comparePoints)
                {
                    double distance = Math.Sqrt(Math.Pow(referencePoint.X - comparePoint.X, 2) +
                                               Math.Pow(referencePoint.Y - comparePoint.Y, 2) +
                                               Math.Pow(referencePoint.Z - comparePoint.Z, 2));
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                    }
                }

                distances.Add(minDistance);
            }

            // Sort the distances in ascending order
            distances.Sort();

            // Calculate the index of the 95th percentile (95% of the distances)
            int index = (int)Math.Ceiling(0.95 * distances.Count) - 1;

            // Return the distance at the 95th percentile
            return distances[index];
        }

    

    class Point
        {
            public double X { get; set; }
            public double Y { get; set; }
            public double Z { get; set; }

            public Point(double x, double y, double z)
            {
                X = x;
                Y = y;
                Z = z;
            }
        }
    }   
}
