# AI_struct_eval
ESAPI script

This project was created to automate the evaluation of two structure sets in our case relevant for our AI segmentation quality assesments.

The script creates initially an openfile dialog that requires a .txt file structure as exemplified below
```
exportdir;\\Client\C$\AI_eval\HN_data2
PatientID1;Primær
..
PatientID10;Primær
PatientID11;Primær
```

The first line tells where to store the results

The subsequent lines refers to the patient to open and what substring to look for in identifying a relevant structureset. The name of the AI structures set is currently hardcoded in the script but can easily be changed.


